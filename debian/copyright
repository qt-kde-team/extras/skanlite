Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Skanlite
Upstream-Contact: Kåre Särs <kare.sars@iki.fi>
Source: https://download.kde.org/

Files: *
Copyright: 2007-2012, Kåre Särs <kare.sars@iki.fi>
           2009, Arseniy Lartsev <receive-spam@yandex.ru
           2014, Gregor Mitsch <codeminister@publicstatic.de>
           2017, Alexander Trufanov <trufanovan@gmail.com>
           2018, Alexander Volkov <a.volkov@rusbitech.ru>
License: GPL-2+3+KDEeV

Files: debian/*
Copyright: 2009-2012, Kai Wasserbäch <curan@debian.org>
           2009-2013, Mark Purcell <msp@debian.org>
           2016-2024, Debian Qt/KDE Maintainers <debian-qt-kde@lists.debian.org>
License: GPL-2+3+

Files: doc/*
Copyright: 2008-2013, Kåre Särs <kare.sars@iki.fi>
           2008, Anne-Marie Mahfouf <annma@kde.org>
License: GFDL-1.3

Files: src/org.kde.skanlite.appdata.xml
Copyright: 2016, Burkhard Lück <lueck@hube-lueck.de>
License: CC0-1.0

License: CC0-1.0
 To the extent possible under law, the author(s) have dedicated all copyright
 and related and neighboring rights to this software to the public domain
 worldwide. This software is distributed without any warranty.
 .
 You should have received a copy of the CC0 Public Domain Dedication along
 with this software. If not, see
 <https://creativecommons.org/publicdomain/zero/1.0/>.
 .
 On Debian systems, the complete text of the CC0 Public Domain Dedication
 can be found in "/usr/share/common-licenses/CC0-1.0".

License: GFDL-1.3
 Permission is granted to copy, distribute and/or modify this document
 under the terms of the GNU Free Documentation License, Version 1.3 
 or any later version published by the Free Software Foundation;
 with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
 A copy of the license is included in the section entitled.
 .
 On Debian systems, the complete text of the GNU Free Documentation License
 version 1.3 can be found in "/usr/share/common-licenses/GFDL-1.3"

License: GPL-2+3+
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the 
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License.
 along with this program.  If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2"
 and the complete text of the GNU General Public License version 3
 can be found in "/usr/share/common-licenses/GPL-3".

License: GPL-2+3+KDEeV
 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of
 the License or (at your option) version 3 or any later version
 accepted by the membership of KDE e.V. (or its successor approved
  by the membership of KDE e.V.), which shall act as a proxy
 defined in Section 14 of version 3 of the license.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License.
 along with this program.  If not, see <http://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in "/usr/share/common-licenses/GPL-2"
 and the complete text of the GNU General Public License version 3
 can be found in "/usr/share/common-licenses/GPL-3".
